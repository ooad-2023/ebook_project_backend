import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-Bill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';
import { Repository } from 'typeorm';
import { Ebook } from 'src/ebooks/entities/ebook.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
    @InjectRepository(Customer)
    private customersRepositiry: Repository<Customer>,
    @InjectRepository(Ebook)
    private ebooksRepository: Repository<Ebook>,
  ) {}
  async create(createBillDto: CreateBillDto) {
    const customer = await this.customersRepositiry.findOneBy({
      id: createBillDto.customerId,
    });
    const bill: Bill = new Bill();
    // bill.createdDate = createBillDto.createdDate;
    bill.buy = createBillDto.buy;
    bill.change = createBillDto.change;
    bill.total = createBillDto.buy - createBillDto.change;
    bill.customer = customer;
    await this.billsRepository.save(bill);
    return await this.billsRepository.findOne({
      where: { id: bill.id },
      relations: ['billDetail'],
    });
  }

  findAll() {
    return this.billsRepository.find({
      relations: ['customer', 'billDetail', 'billDetail.ebook'],
    });
  }

  async findOne(id: number) {
    const bill = this.billsRepository.findOne({
      where: { id: id },
      relations: ['customer', 'billDetail', 'billDetail.ebook'],
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billsRepository.findOne({ where: { id: id } });
  }

  updateBill() {
    return '';
  };

  async remove(id: number) {
    const bill = await this.billsRepository.findOne({
      where: { id: id },
    });
    if (!bill) {
      throw new NotFoundException();
    } else {
      await this.billsRepository.softRemove(bill);
    }
    return bill;
  }
  showBillAll = async (id: string) => {
    const ebook = await this.ebooksRepository.findOne({ where: { id: +id } });
  };
}
