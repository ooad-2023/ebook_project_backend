import { Module } from '@nestjs/common';
import { BillsService } from './bills.service';
import { BillsController } from './bills.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';
import { Ebook } from 'src/ebooks/entities/ebook.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, Ebook, Customer])],
  controllers: [BillsController],
  providers: [BillsService],
})
export class BillsModule {}
