import { Cart } from 'src/carts/entities/cart.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn({ name: 'bill_id' })
  id: number;

  @Column({ name: 'bill_buy' })
  buy: number;

  @Column({ name: 'bill_change' })
  change: number;

  @Column({ name: 'bill_total' })
  total: number;

  // @OneToMany(() => BillDetail, (billDetail) => billDetail.bill)
  // @JoinColumn()
  // billDetail: BillDetail[];

  @ManyToOne(() => Customer, (customer) => customer.bills)
  @JoinColumn({ name: 'customer_id' })
  customer: Customer;

  @ManyToOne(() => Cart, (cart) => cart.bill)
  @JoinColumn({ name: 'cart_id' }) 
  cart: Cart;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;
}
