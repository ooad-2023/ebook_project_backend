import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateBillDto {
  @IsNumber()
  @IsNotEmpty()
  buy: number;

  @IsNumber()
  @IsNotEmpty()
  change: number;

  @IsNumber()
  @IsNotEmpty()
  total: number;

  @IsNumber()
  @IsNotEmpty()
  customerId: number;
}
