import { Module } from '@nestjs/common';
import { CartsService } from './carts.service';
import { CartsController } from './carts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cart } from './entities/cart.entity';
import { CartItem } from './entities/cart-item.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Ebook } from 'src/ebooks/entities/ebook.entity';
import { Bill } from 'src/bills/entities/bill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Cart, CartItem, Customer, Ebook, Bill])],
  controllers: [CartsController],
  providers: [CartsService],
})
export class CartsModule {}
