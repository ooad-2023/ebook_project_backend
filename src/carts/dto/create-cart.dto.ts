import { IsNotEmpty } from 'class-validator';
import { Cart } from '../entities/cart.entity';
import { Ebook } from 'src/ebooks/entities/ebook.entity';

class CreatedCartItemDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  ebookId: number;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  author: number;

  @IsNotEmpty()
  cover: number;

  @IsNotEmpty()
  file: number;

  @IsNotEmpty()
  cart?: Cart;

  @IsNotEmpty()
  ebook?: Ebook;

  @IsNotEmpty()
  createdDate?: Date;

  @IsNotEmpty()
  updatedDate?: Date;

  @IsNotEmpty()
  deletedDate?: Date;
}

export class CreateCartDto {
  // @IsNotEmpty()
  // customerId: number;

  // @IsNotEmpty()
  // recieved: number;

  // @IsNotEmpty()
  // change: number;
  
  // @IsNotEmpty()
  // payment: string;

  // @IsNotEmpty()
  // total: number;

  @IsNotEmpty()
  cartItems: CreatedCartItemDto[];
}
