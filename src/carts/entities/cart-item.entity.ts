import { Ebook } from 'src/ebooks/entities/ebook.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';
import { Cart } from './cart.entity';

@Entity()
export class CartItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({ type: 'float' })
  price: number;

  @Column({ type: 'float' })
  total: number;

  @Column({ length: '50' })
  author: string;

  @Column({
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  imageCover: string;

  @Column({
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  imageFile: string;

  @ManyToOne(() => Cart, (cart) => cart.cartItems)
  cart: Cart;

  @ManyToOne(() => Ebook, (ebook) => ebook.cartItems)
  ebook: Ebook;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
