import { Injectable } from '@nestjs/common';
import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from './entities/cart.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Ebook } from 'src/ebooks/entities/ebook.entity';
import { CartItem } from './entities/cart-item.entity';
import { Repository } from 'typeorm';
import { Bill } from 'src/bills/entities/bill.entity';

@Injectable()
export class CartsService {
  constructor(
    @InjectRepository(Cart)
    private cartsRepository: Repository<Cart>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Ebook)
    private ebooksRepository: Repository<Ebook>,
    @InjectRepository(CartItem)
    private cartItemsRepository: Repository<CartItem>,
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
  ) {}
  async create(createCartDto: CreateCartDto) {
    // const customer = await this.customersRepository.findOneBy({
    //   id: createCartDto.customerId,
    // });
    const cart: Cart = new Cart();
    // cart.customer = customer;
    // cart.recieved = createCartDto.recieved;
    // cart.change = createCartDto.recieved - createCartDto.total;
    // cart.payment = createCartDto.payment;
    cart.total = 0;
    await this.cartsRepository.save(cart); 

    for (const cit of createCartDto.cartItems) {
      const cartItem = new CartItem();
      cartItem.ebook = await this.ebooksRepository.findOneBy({
        id: cit.ebookId,
      });
      cartItem.name = cartItem.ebook.name;
      cartItem.price = cartItem.ebook.price;
      cartItem.total = cartItem.price
      cartItem.author = cartItem.ebook.author;
      cartItem.imageCover = cartItem.ebook.imageCover;
      cartItem.imageFile = cartItem.ebook.imageFile;
      cartItem.cart = cart;
      await this.cartItemsRepository.save(cartItem);
      cart.total = cart.total + cartItem.total;
    }
    await this.cartsRepository.save(cart);

    const bill = new Bill();
    bill.cart = cart;
    await this.billsRepository.save(bill);

    return await this.cartsRepository.findOne({
      where: { id: cart.id },
      relations: ['cartItems'],
    });
  }

  findAll() {
    return this.cartsRepository.find({
      order: {
        id: 'DESC'
      },
      relations: ['cartItems'],
    });
  }

  findAllAsc() {
    return this.cartsRepository.find({
      relations: ['customer', 'cartItems'],
      order: { createdAt: 'ASC' },
    });
  }

  findOne(id: number) {
    return this.cartsRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateCartDto: UpdateCartDto) {
    return `This action updates a #${id} cart`;
  }

  async remove(id: number) {
    const cart = await this.cartsRepository.findOneBy({ id: id });
    return this.cartsRepository.softRemove(cart);
  }
}
