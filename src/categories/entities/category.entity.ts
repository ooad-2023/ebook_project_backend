import { Ebook } from 'src/ebooks/entities/ebook.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Category {
  @PrimaryGeneratedColumn({ name: 'catagory_id' })
  id: number;

  @Column({ name: 'catagory_name', unique: true })
  name: string;

  @OneToMany(() => Ebook, (ebook) => ebook.category)
  ebook: Ebook[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
