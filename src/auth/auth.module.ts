import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { CustomersModule } from 'src/customers/customers.module';
import { CustomersService } from 'src/customers/customers.service';

@Module({
  imports: [
    CustomersModule,
    PassportModule,
    JwtModule.register({ 
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1w' },
    }),
    TypeOrmModule.forFeature([Customer]),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy, JwtService, CustomersService],
  exports: [AuthService],
})
export class AuthModule {}
