import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Customer } from 'src/customers/entities/customer.entity';
import { CustomersService } from 'src/customers/customers.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly customersService: CustomersService,
  ) {}

  async validateCustomer(email: string, password: string): Promise<any> {
    const customer = await this.customersService.findOneByEmail(email);
    if (!customer) {
      return null;
    }
    const isMatch = await bcrypt.compare(password, customer.password);
    if (!isMatch) {
      return null;
    }
    return customer;
  }
  

  async login(customer: any) {
    const payload = { email: customer.email, sub: customer.id };
    return {
      customer,
      access_token: this.jwtService.sign(payload),
    };
  }
}
