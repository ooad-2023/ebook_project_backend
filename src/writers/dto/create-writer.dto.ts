import { IsNotEmpty, Length } from "class-validator";

export class CreateWriterDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  bankNumber: string;

  @IsNotEmpty()
  bankName: string;
}
