import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Writer {
  @PrimaryGeneratedColumn({ name: 'writer_id'})
  id: number;

  @Column({ name: 'writer_name', length: '100' })
  name: string;

  @Column({ name: 'writer_email', unique: true })
  email: string;

  @Column({ name: 'writer_password' })
  password: string;

  @Column({ name: 'writer_tel', unique: true })
  tel: string;

  @Column({ name: 'writer_bank_number', unique: true })
  bankNumber: string;

  @Column({ name: 'writer_bank_name' })
  bankName: string;

  @CreateDateColumn({ name: 'writer_register_date' })
  registerDate: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
