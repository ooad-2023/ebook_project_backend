import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateWriterDto } from './dto/create-writer.dto';
import { UpdateWriterDto } from './dto/update-writer.dto';
import { Writer } from './entities/writer.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class WritersService {
  constructor(
    @InjectRepository(Writer)
    private writersRepository: Repository<Writer>,
  ) {}

  create(createWriterDto: CreateWriterDto) {
    const newWriter = new Writer();
    newWriter.name = newWriter.name;
    newWriter.email = newWriter.email;
    newWriter.password = newWriter.password;
    newWriter.tel = newWriter.tel;
    newWriter.bankNumber = newWriter.bankNumber;
    newWriter.bankName = newWriter.bankName;
    return this.writersRepository.save(createWriterDto);
  }

  findAll() {
    return this.writersRepository.find({});
  }

  async findOne(id: number) {
    const customer = await this.writersRepository.findOne({
      where: { id: id },
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateWriterDto: UpdateWriterDto) {
    const writer = await this.writersRepository.findOneBy({ id: id });
    if (!writer) {
      throw new NotFoundException();
    }
    const updateWriter = { ...writer, ...updateWriterDto };
    return this.writersRepository.save(updateWriter);
  }

  async remove(id: number) {
    const writer = await this.writersRepository.findOneBy({ id: id });
    if (!writer) {
      throw new NotFoundException();
    }
    return this.writersRepository.softRemove(writer);
  }
}
