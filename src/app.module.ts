import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EbooksModule } from './ebooks/ebooks.module';
import { CategoriesModule } from './categories/categories.module';
import { Ebook } from './ebooks/entities/ebook.entity';
import { Category } from './categories/entities/category.entity';
import { CartsModule } from './carts/carts.module';
import { AdminsModule } from './admins/admins.module';
import { BillsModule } from './bills/bills.module';
import { PaymentsModule } from './payments/payments.module';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Admin } from './admins/entities/admin.entity';
import { Bill } from './bills/entities/bill.entity';
import { Cart } from './carts/entities/cart.entity';
import { Payment } from './payments/entities/payment.entity';
import { DataSource } from 'typeorm';
import { Writer } from './writers/entities/writer.entity';
import { WritersModule } from './writers/writers.module';
import { CartItem } from './carts/entities/cart-item.entity';
import { FavoriteModule } from './favorite/favorite.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './app.sqlite',
      // type: 'sqlite',
      // database: './app.sqlite',
      entities: [Ebook, Category, Customer, Writer, Admin, Cart, CartItem, Bill, Payment ],
      synchronize: true,
    }),
    // TypeOrmModule.forRoot({
    //   type: 'mysql',
    //   host: 'localhost',
    //   port: 3306,
    //   username: 'ebook',
    //   password: 'Pass@1234',
    //   database: 'ebook',
    //   entities: [Ebook, Category, Customer, Writer, Admin, Cart, CartItem, Bill, BillDetail, Payment ],
    //   synchronize: true,
    // }),
    AuthModule,
    EbooksModule,
    CategoriesModule,
    CustomersModule,
    WritersModule,
    AdminsModule,
    CartsModule,
    BillsModule,
    PaymentsModule,
    FavoriteModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
