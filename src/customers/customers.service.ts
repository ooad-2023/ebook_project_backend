import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    const newCustomer = new Customer();
    newCustomer.name = newCustomer.name;
    newCustomer.surname = newCustomer.surname;
    newCustomer.email = newCustomer.email;
    newCustomer.password = newCustomer.password;
    newCustomer.tel = newCustomer.tel;
    newCustomer.birthday = newCustomer.birthday;
    newCustomer.point = newCustomer.point;
    return this.customersRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customersRepository.find({});
  }

  async findOne(id: number) {
    const customer = await this.customersRepository.findOne({
      where: { id: id },
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async findOneByEmail(email: string) {
    try {
      const customer = await this.customersRepository.findOne({
        where: { email },
      });
      return customer; 
    } catch (e) {
      console.log(e);
      throw e;
    }
}

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customersRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...customer, ...updateCustomerDto };
    return this.customersRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const customer = await this.customersRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customersRepository.softRemove(customer);
  }
}
