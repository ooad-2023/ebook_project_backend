// import { Order } from 'src/orders/entities/order.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { Cart } from 'src/carts/entities/cart.entity';
import { Favorite } from 'src/favorite/entities/favorite.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Customer {
  @PrimaryGeneratedColumn({ name: 'customer_id' })
  id: number;

  @Column({ name: 'customer_name' })
  name: string;

  @Column({ name: 'customer_surname' })
  surname: string;

  @Column({ name: 'customer_email', unique: true })
  email: string;

  @Column({ name: 'customer_password' })
  password: string;

  @Column({ name: 'customer_tel', unique: true })
  tel: string;

  @Column({ name: 'customer_birthday'})
  birthday: string;

  @Column({ name: 'customer_point' })
  point: number;

  @OneToMany(() => Cart, (cart) => cart.customer)
  cart: Cart[];

  // @OneToMany(() => Favorite, (favorite) => favorite.customer)
  // favorite: Favorite[];

  @OneToMany(() => Bill, (bill) => bill.customer)
  bills: Bill;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
