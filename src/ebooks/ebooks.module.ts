import { Module } from '@nestjs/common';
import { EbooksService } from './ebooks.service';
import { EbooksController } from './ebooks.controller';
import { CategoriesModule } from 'src/categories/categories.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { Ebook } from './entities/ebook.entity';

@Module({
  imports: [CategoriesModule, TypeOrmModule.forFeature([Ebook, Category])],
  controllers: [EbooksController],
  providers: [EbooksService],
})
export class EbooksModule {}
