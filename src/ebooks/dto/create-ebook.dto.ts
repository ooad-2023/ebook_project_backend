import { IsNotEmpty, Length } from 'class-validator';

export class CreateEbookDto {
  @IsNotEmpty()
  @Length(3, 100)
  name: string;

  @IsNotEmpty()
  author: string;

  @IsNotEmpty()
  content: string;

  @IsNotEmpty()
  imageCover = 'No-Image-Placeholder.jpg';

  @IsNotEmpty()
  imageFile = 'No-Image-Placeholder.jpg';

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  categoryId: number;
}
