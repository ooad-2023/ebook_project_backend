import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
  Query,
  BadRequestException,
  UploadedFiles,
} from '@nestjs/common';
import { EbooksService } from './ebooks.service';
import { CreateEbookDto } from './dto/create-ebook.dto';
import { UpdateEbookDto } from './dto/update-ebook.dto';
import { FileFieldsInterceptor, FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { Response, query } from 'express';
@Controller('ebooks')
export class EbooksController {
  constructor(private readonly ebooksService: EbooksService) {}
  @Post()
@UseInterceptors(
  FileFieldsInterceptor([
    { name: 'cover', maxCount: 1 },
    { name: 'file', maxCount: 1 },
  ], {
    storage: diskStorage({
      destination: (req, file, cb) => {
        cb(null, file.fieldname === 'cover' ? './ebook_covers' : './ebook_files');
      },
      filename: (req, file, cb) => {
        const name = uuidv4();
        cb(null, name + extname(file.originalname));
      },
    }),
  }),
)
create(
  @Body() createEbookDto: CreateEbookDto,
  @UploadedFiles() files: Record<string, Express.Multer.File[]>,
) {
  createEbookDto.imageCover = files['cover'][0].filename;
  createEbookDto.imageFile = files['file'][0].filename;
  return this.ebooksService.create(createEbookDto);
}

  @Get()
  findAll() {
    return this.ebooksService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ebooksService.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const ebook = await this.ebooksService.findOne(+id);
    res.sendFile(ebook.imageCover, { root: './ebook_covers' });
  }

  @Get('image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './ebook_covers' });
  }

  @Get('category/:categoryId')
  findByCategory(@Param('categoryId') categoryId: number) {
    return this.ebooksService.findByCategory(categoryId);
  }

  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './ebook_covers',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.ebooksService.update(+id, { imageCover: file.filename });
  }
  
  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './ebook_covers',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  async update(
    @Param('id') id: string,
    @Body() updateEbookDto: UpdateEbookDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateEbookDto.imageCover = file.filename;
    }
    return await this.ebooksService.update(+id, updateEbookDto);
  }
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ebooksService.remove(+id);
  }
}
