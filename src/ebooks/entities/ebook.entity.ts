import { CartItem } from 'src/carts/entities/cart-item.entity';
import { Cart } from 'src/carts/entities/cart.entity';
import { Category } from 'src/categories/entities/category.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Ebook {
  [x: string]: any;
  @PrimaryGeneratedColumn({ name: 'book_id' })
  id: number;

  @Column({ name: 'book_name', length: '100' })
  name: string;

  @Column({ name: 'book_author',length: '50' })
  author: string;

  @Column({ name: 'book_content',length: '200' })
  content: string;

  @Column({ 
    name: 'book_cover',
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })  
  imageCover: string;

  @Column({ 
    name: 'book_file',
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })  
  imageFile: string;

  @Column({ name: 'book_price', type: 'float'})
  price: number;

  @CreateDateColumn()
  uploaddate: Date;

  @Column()
  categoryId: number;

  @ManyToOne(() => Category, (category) => category.ebook)
  category: Category;

  @OneToMany(() => CartItem, (cartItems) => cartItems.ebook)
  cartItems: CartItem;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
