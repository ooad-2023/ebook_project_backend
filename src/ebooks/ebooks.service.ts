import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEbookDto } from './dto/create-ebook.dto';
import { UpdateEbookDto } from './dto/update-ebook.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Ebook } from './entities/ebook.entity';
import { Like, Repository } from 'typeorm';
import { Category } from 'src/categories/entities/category.entity';

@Injectable()
export class EbooksService {
  constructor(
    @InjectRepository(Ebook)
    private ebooksRepository: Repository<Ebook>,
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}
  
  async create(createEbookDto: CreateEbookDto) {
    const category = await this.categoriesRepository.findOne({
      where: { id: createEbookDto.categoryId },
    });
    const newEbook = new Ebook();
    newEbook.name = createEbookDto.name;
    newEbook.author = createEbookDto.author;
    newEbook.content = createEbookDto.content;
    newEbook.imageCover = createEbookDto.imageCover;
    newEbook.imageFile = createEbookDto.imageFile;
    newEbook.price = createEbookDto.price;
    newEbook.category = category;
    return this.ebooksRepository.save(newEbook);
  }

  findAll() {
    return this.ebooksRepository.find({});
  }

  // async findAll(query) {
  //   const page = query.page || 1;
  //   const take = query.take || 10;
  //   const skip = (page - 1) * take;
  //   const keyword = query.keyword || '';

  //   const [result, total] = await this.ebooksRepository.findAndCount({
  //     relations: ['category'],
  //     where: { name: Like(`%${keyword}%`) },
  //     take: take,
  //     skip: skip,
  //   });

  //   const lastPage = Math.ceil(total / take);
  //   return {
  //     data: result,
  //     count: total,
  //     currentPage: page,
  //     lastPage: lastPage,
  //   };
  // }

  findByCategory(categoryId: number) {
    return this.ebooksRepository.find({
      where: { categoryId: categoryId },
      relations: { category: true },
      order: {name: 'ASC'}
    });
  }

  findOne(id: number) {
    const ebook = this.ebooksRepository.findOne({
      where: { id: id },
      relations: ['category'],
    });
    if (!ebook) {
      throw new NotFoundException();
    }
    return this.ebooksRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateEbookDto: UpdateEbookDto) {
    const ebook = await this.ebooksRepository.findOneBy({ id: id });
    if (!ebook) {
      throw new NotFoundException();
    }
    const updatedEbook = { ...ebook, ...updateEbookDto };
    return this.ebooksRepository.save(updatedEbook);
  }

  async remove(id: number) {
    const ebook = await this.ebooksRepository.findOneBy({ id: id });
    if (!ebook) {
      throw new NotFoundException();
    }
    return this.ebooksRepository.softRemove(ebook);
  }
}
