import { Customer } from "src/customers/entities/customer.entity";
import { CreateDateColumn, DeleteDateColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export class Favorite {
  @PrimaryGeneratedColumn()
  id: number;

  // @ManyToOne(() => Customer, (customer) => customer.favorite)
  // customer: Customer;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
